﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Transactions;

namespace SafeSynTools
{
    public partial class Form1 : Form
    {
        int seconds_time = Convert.ToInt32(ConfigurationManager.AppSettings["seconds_time"]);//获取实时同步的执行间隔时间
        string errorLogPath = System.Environment.CurrentDirectory + "\\errorlog.txt";//错误日志记录路径
        string successLogPath = System.Environment.CurrentDirectory + "\\successlog.txt";//成功日志记录路径
        string reportConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_report"].ToString();//report
        string sql_share = ConfigurationManager.ConnectionStrings["sqlConnectionString_share"].ToString();
        string sql_totalIn = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalIn"].ToString();
        string sql_totalOrder = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalorder"].ToString();
        string sql_totalOut = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalOut"].ToString();
        System.Timers.Timer timer = new System.Timers.Timer();
        System.Timers.Timer timer_clear = new System.Timers.Timer();
        string add_date_s = "";
        string add_date_e = "";
        string add_date_m = "";
        int DBtype = Convert.ToInt32(ConfigurationManager.AppSettings["DBtype"]);

        public Form1()
        {
            InitializeComponent();
            seconds_time = seconds_time * 1000;//秒
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            //创建实时同步的线程
            timer.AutoReset = false;
            timer.Interval = seconds_time;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(synBySeconds);
            timer.Start();

            timer_clear.AutoReset = false;
            timer_clear.Interval = 10000;
            timer_clear.Elapsed += new System.Timers.ElapsedEventHandler(clear_Elapse);
            timer_clear.Start();
        }

        #region 实时同步的方法
        private void synBySeconds(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                add_date_s = System.DateTime.Now.ToString("yyyy-MM") + "-01";
                add_date_e = System.DateTime.Now.ToString("yyyy-MM-dd");
                add_date_m = System.DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");    //中獎金額冷熱表的切割點
                if ((DateTime.Now.ToString("dd") == "01" || DateTime.Now.ToString("dd") == "02" || DateTime.Now.ToString("dd") == "03") && DateTime.Now.Hour == 1 && DateTime.Now.Minute < 10)
                {
                    add_date_s = System.DateTime.Now.AddMonths(-1).ToString("yyyy-MM") + "-01";
                    add_date_e = System.DateTime.Now.AddDays((DateTime.Now.ToString("dd") == "01" ? -1 : (DateTime.Now.ToString("dd") == "02" ? -2 : -3))).ToString("yyyy-MM-dd");
                    add_date_m = System.DateTime.Now.AddDays((DateTime.Now.ToString("dd") == "01" ? -2 : (DateTime.Now.ToString("dd") == "02" ? -3 : -4))).ToString("yyyy-MM-dd");
                }
                DataTable dt = new DataTable();
                string tenant_str = "select identityid,CONVERT(varchar(10),'" + add_date_s + "',121) as add_date from dt_tenant where DBtype='" + DBtype + "' order by identityid";
                dt = SqlDbHelper.GetQuery(tenant_str, null, sql_share);
                SafeSynTools.DBHelper.Model ModelHelper = new DBHelper.Model();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt_month_all_record dt_month = new dt_month_all_record();
                    ModelHelper.ConvertDataRowToModel(dt.Rows[i], dt_month);
                    StaticList.List_dt_month_all_record.Add(dt_month);
                }

                System.Threading.Tasks.Task task3 = new System.Threading.Tasks.Task(syn_dt_out_account);
                task3.Start();

                System.Threading.Tasks.Task task2 = new System.Threading.Tasks.Task(syn_dt_in_account);
                task2.Start();

                System.Threading.Tasks.Task task1 = new System.Threading.Tasks.Task(syn_dt_lottery_orders);
                task1.Start();

                task3.Wait();
                task2.Wait();
                task1.Wait();

                if (task3.Status == System.Threading.Tasks.TaskStatus.RanToCompletion)
                    task3.Dispose();
                if (task2.Status == System.Threading.Tasks.TaskStatus.RanToCompletion)
                    task2.Dispose();
                if (task1.Status == System.Threading.Tasks.TaskStatus.RanToCompletion)
                    task1.Dispose();

                List<dt_month_all_record> ListDtMonthAll = StaticList.List_dt_month_all_record;
                DataTable dttest = ModelHelper.ConvertListToDataTable(ListDtMonthAll);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                    {
                        Thread.Sleep(1000 * 60 * 60 * 3);
                        continue;
                    }
                    string identityid = dt.Rows[i]["identityid"].ToString();
                    if (CheckData(identityid, add_date_s, add_date_e) == true)    //找total-order遠程機有沒有資料
                    {
                        RunSqlBulkCopy(dttest.Select("identityid='" + identityid + "'")[0], "dt_month_all_record",sql_totalOrder);
                    }
                    else
                    {
                        RunSqlBulkUpdate(dttest.Select("identityid='" + identityid + "'")[0], "dt_month_all_record", sql_totalOrder);
                    }
                    System.Threading.Thread.Sleep(800);
                }
                FillMsg("更新完成 ----------" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                WriteSuccessLog("更新完成 ----------" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                StaticList.List_dt_month_all_record.Clear();
                timer.Start();
            }
        }

        void syn_dt_in_account()
        {
            string querystr = "";
            try
            {
                List<SqlParameter> Insert_Parameter = new List<SqlParameter>();
                SqlParameter Parameter_date_s = new SqlParameter("@add_time_s", SqlDbType.Date);
                Parameter_date_s.Value = add_date_s;
                SqlParameter Parameter_date_e = new SqlParameter("@add_time_e", SqlDbType.Date);
                Parameter_date_e.Value = add_date_e;
                SqlParameter Parameter_date_m = new SqlParameter("@add_time_m", SqlDbType.Date);
                Parameter_date_m.Value = add_date_m;
                Insert_Parameter.Add(Parameter_date_s);
                Insert_Parameter.Add(Parameter_date_e);
                Insert_Parameter.Add(Parameter_date_m);
                DataTable dt = new DataTable();

                string ali_in = "";
                string we_in = "";
                string qq_in = "";
                string fastpay_in = "";
                string unionpay_in = "";
                string sql_str = "select Alipay from dbo.dt_diction_quickpay where Alipay>0";
                dt = SqlDbHelper.GetQuery(sql_str, null, sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ali_in += dt.Rows[i]["Alipay"] + ",";
                }
                sql_str = "select Wechat from dbo.dt_diction_quickpay where Wechat>0";
                dt = SqlDbHelper.GetQuery(sql_str, null, sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    we_in += dt.Rows[i]["Wechat"] + ",";
                }
                sql_str = "select QQ from dbo.dt_diction_quickpay where QQ>0";
                dt = SqlDbHelper.GetQuery(sql_str, null, sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    qq_in += dt.Rows[i]["QQ"] + ",";
                }
                sql_str = "select FASTPAY from dbo.dt_diction_quickpay where FASTPAY>0";
                dt = SqlDbHelper.GetQuery(sql_str, null, sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    fastpay_in += dt.Rows[i]["FASTPAY"] + ",";
                }
                sql_str = "select Unionpay from dbo.dt_diction_quickpay where Unionpay>0";
                dt = SqlDbHelper.GetQuery(sql_str, null, sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    unionpay_in += dt.Rows[i]["Unionpay"] + ",";
                }

                List<dt_month_all_record> dt_month_all_record = new List<dt_month_all_record>();
                SafeSynTools.DBHelper.Model ModelHelper = new DBHelper.Model();

                //金額
                querystr = " select  identityid,isnull(sum(czmony),0) as czmony,isnull(sum(operattime),0) as operattime,isnull(sum(fastpaymoney),0) as fastpaymoney,isnull(sum(bankmony),0) as bankmony,isnull(sum(aliypaymony),0) as aliypaymony,isnull(sum(wechatmony),0) as wechatmony,isnull(sum(rgckmony),0) as rgckmony ,isnull(sum(hdljmony),0) as hdljmony,isnull(sum(xthdmony),0) as xthdmony,isnull(sum(qthdmony),0) as qthdmony,isnull(sum(dlgzmony),0) as dlgzmony,isnull(sum(dlfhmony),0) as dlfhmony,isnull(sum(czcount),0) as czcount,isnull(sum(qqmoney),0) as qqmoney,isnull(sum(fourthmoney),0) as fourthmoney,isnull(sum(unionpaymoney),0) as unionpaymoney  from dbo.dt_user_in_sum with(index(add_date)) where add_date >=@add_time_s and add_date <=@add_time_e    group by identityid";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }
                querystr = "select  identityid,isnull(sum(zjmoney),0) as zjmoney  from dbo.dt_lottery_winning_sum with(index(add_date)) where add_date >=@add_time_s and add_date <=@add_time_e group by identityid";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select  identityid,isnull(sum(rebatemoney),0) as rebatemoney from dbo.dt_user_get_point_sum with(index(add_date)) where add_date >=@add_time_s and add_date <=@add_time_e    group by identityid   ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                //人數
                querystr = "  select identityid,count(distinct(user_id ))  as cznumber from dbo.dt_user_in_record with(index(type_add_date)) where type = 1 and state = 1 and  add_date >=@add_time_s and add_date <=@add_time_e group by identityid   ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select identityid,count(distinct(user_id )) as banknumber from dbo.dt_user_in_record with(index(type2))  where type2 = 21 and state = 1 and  add_date >=@add_time_s and add_date <=@add_time_e group by identityid  ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select identityid,count(distinct(user_id )) as rgcknumber from dbo.dt_user_in_record with(index(type2)) where type2 in (20) and[state] = 1 and  add_date >=@add_time_s and add_date <=@add_time_e group by identityid              ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select identityid,count(distinct(user_id )) as hdljnumber from dbo.dt_user_in_record with(index(type2)) where type2 in(7, 12) and[state] = 1 and  add_date >=@add_time_s and add_date <=@add_time_e group by identityid    ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select identityid,count(distinct(user_id )) as xthdnumber from dbo.dt_user_in_record with(index(type2))  where  type2 = 7 and[state] = 1 and  add_date >=@add_time_s and add_date <=@add_time_e group by identityid      ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select identityid,count(distinct(user_id )) as qthdnumber from dbo.dt_user_in_record with(index(type2)) where type2 = 12 and[state] = 1 and  add_date >=@add_time_s and add_date <=@add_time_e group by identityid   ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                //querystr = "select identityid,count(distinct(user_id )) as cdnumber from dbo.dt_user_in_record with(nolock)  where type2 = 17  and add_date >=@add_time_s and add_date <=@add_time_e group by identityid             ";
                //dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    lock (StaticList.List_dt_month_all_record)
                //        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                //}
                //代理工資.分紅無計算
                //querystr = "select identityid,count(distinct(user_id )) as dlgznumber from dbo.dt_user_in_record with(nolock)  where type2 = 14  and[state] = 1 and add_date >=@add_time_s and add_date <=@add_time_e group by identityid   ";
                //dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    lock (StaticList.List_dt_month_all_record)
                //        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                //}

                //querystr = "select identityid,count(distinct(user_id )) as dlfhnumber from dbo.dt_user_in_record with(nolock)  where type2 = 13  and[state] = 1 and  add_date >=@add_time_s and add_date <=@add_time_e group by identityid   ";
                //dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    lock (StaticList.List_dt_month_all_record)
                //        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                //}

                querystr = " select identityid,count(distinct(user_id)) as fastnumber from dbo.dt_user_in_record with(index(type2))  where add_date>=@add_time_s and add_date<=@add_time_e  and type2 in (" + fastpay_in.Substring(0, fastpay_in.Length - 1) + ") and [state]=1 group by identityid   ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = " select identityid,count(distinct(user_id)) as aliypaynumber from dbo.dt_user_in_record with(index(type2))  where add_date>=@add_time_s and add_date<=@add_time_e  and type2 in(" + ali_in + "22) and [state]=1 group by identityid   ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = " select identityid,count(distinct(user_id)) as wechatnumber from dbo.dt_user_in_record with(index(type2))  where  add_date>=@add_time_s and add_date<=@add_time_e  and type2 in(" + we_in + "23) and [state]=1 group by identityid  ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = " select identityid,count(distinct(user_id)) as qqnumber from dbo.dt_user_in_record with(index(type2)) where  add_date >= @add_time_s and add_date<= @add_time_e and type2 in(" + qq_in + "201) group by identityid  ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = " select identityid,count(distinct(user_id)) as fourthnumber from dbo.dt_user_in_record with(index(type2)) where  add_date >= @add_time_s and add_date<= @add_time_e and type2 in(268) group by identityid  ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = " select identityid,count(distinct(user_id)) as unionpaynumber from dbo.dt_user_in_record with(index(type2)) where  add_date >= @add_time_s and add_date<= @add_time_e and type2 in(" + unionpay_in + "291) group by identityid  ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                //querystr = " select identityid,count(distinct(user_id)) as zjnumber from dbo.dt_lottery_winning_userrecord with(nolock) where  add_date >= @add_time_s and add_date<= @add_time_e group by identityid  ";
                if(Convert.ToInt16(add_date_e.Substring(add_date_e.Length-2,2))>=3)
                    querystr = " select identityid,count(distinct(user_id)) as zjnumber from (" +
                               " select identityid,user_id from dbo.dt_lottery_winning_userrecord with(index(add_date)) where add_date >= @add_time_m and add_date<= @add_time_e" +
                               " union select identityid,user_id from dbo.dt_lottery_winning_userrecord_cold with(index(add_date)) where add_date >= @add_time_s and add_date< @add_time_m ) A group by identityid  ";
                else
                    querystr = " select identityid,count(distinct(user_id)) as zjnumber from dbo.dt_lottery_winning_userrecord with(nolock) where  add_date >= @add_time_s and add_date<= @add_time_e group by identityid  ";
                    dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = " select identityid,count(distinct(user_id)) as rebatenumber from dbo.dt_user_get_point_userrecord with(index(add_date)) where  add_date >= @add_time_s and add_date<= @add_time_e group by identityid   ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalIn);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != "未将对象引用设置到对象的实例。")
                {
                    FillErrorMsg("syn_dt_in_account:" + ex);
                    FillErrorMsg("sql语句:" + querystr);
                    WriteErrorLog("syn_dt_in_account:" + DateTime.Now.ToString(), ex.ToString() + "\r\n" + "sql语句:" + querystr);
                }
            }
        }

        void syn_dt_lottery_orders()
        {
            string querystr = "";
            try
            {
                List<SqlParameter> Insert_Parameter = new List<SqlParameter>();
                SqlParameter Parameter_date_s = new SqlParameter("@add_time_s", SqlDbType.Date);
                Parameter_date_s.Value = add_date_s;
                SqlParameter Parameter_date_e = new SqlParameter("@add_time_e", SqlDbType.Date);
                Parameter_date_e.Value = add_date_e;
                Insert_Parameter.Add(Parameter_date_s);
                Insert_Parameter.Add(Parameter_date_e);
                DataTable dt = new DataTable();
                DBHelper.Model ModelHelper = new DBHelper.Model();
                //金額
                querystr = "select identityid,isnull(sum(tzmony), 0) as tzmony,isnull(sum(tzcount), 0) as tzcount from dt_lottery_sum with(nolock) where add_date >= @add_time_s and add_date<= @add_time_e group by identityid";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOrder);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                //人數
                querystr = "select identityid,count(distinct(user_id)) as tznumber from dbo.dt_lottery_record with(nolock) where add_date>=@add_time_s and add_date<=@add_time_e group by identityid     ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOrder);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != "未将对象引用设置到对象的实例。")
                {
                    FillErrorMsg("syn_dt_lottery_orders:" + ex);
                    FillErrorMsg("sql语句:" + querystr);
                    WriteErrorLog("syn_dt_lottery_orders:" + DateTime.Now.ToString(), ex.ToString() + "\r\n" + "sql语句:" + querystr);
                }
            }
        }

        void syn_dt_out_account()
        {
            string querystr = "";
            try
            {
                List<SqlParameter> Insert_Parameter = new List<SqlParameter>();
                SqlParameter Parameter_date_s = new SqlParameter("@add_time_s", SqlDbType.Date);
                Parameter_date_s.Value = add_date_s;
                SqlParameter Parameter_date_e = new SqlParameter("@add_time_e", SqlDbType.Date);
                Parameter_date_e.Value = add_date_e;
                Insert_Parameter.Add(Parameter_date_s);
                Insert_Parameter.Add(Parameter_date_e);
                DataTable dt = new DataTable();
                DBHelper.Model ModelHelper = new DBHelper.Model();
                //金額
                querystr = " select identityid, isnull(sum(txmony), 0) as txmony,isnull(sum(opearttime), 0) as opearttime,isnull(sum(rgtcmoney), 0) as rgtcmoney,isnull(sum(wctcmony), 0) as wctcmony,isnull(sum(xztcmony), 0) as xztcmony,isnull(sum(jjmony), 0) as jjmony,isnull(sum(txcount), 0) as txcount   from dbo.dt_user_out_sum with(nolock) where add_date >= @add_time_s and add_date <= @add_time_e group by identityid";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOut);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select identityid,isnull(sum(rewardmoney), 0) as rewardmoney,isnull(sum(rewardcount), 0) as rewardcount from dt_reward_sum with(nolock) where add_date >= @add_time_s and add_date<= @add_time_e group by identityid";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOut);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = " select identityid,isnull(sum(cdmony),0) as cdmony from dbo.dt_lottery_cancellationsflow_sum with(nolock) where add_date >= @add_time_s and add_date <= @add_time_e group by identityid";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOut);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }
                //人數
                querystr = "select identityid, count(distinct(user_id))  as txnumber from dbo.dt_user_out_record with(nolock) where type2 in(3)  and[state] = 1 and add_date >= @add_time_s and add_date <= @add_time_e group by identityid ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOut);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select identityid, count(distinct(user_id)) as rgtcnumber from dbo.dt_user_out_record with(nolock) where type2 = 3  and[state] = 1  and add_date >= @add_time_s and add_date <= @add_time_e group by identityid    ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOut);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = " select identityid, count(distinct(user_id)) as wctcnumber from dbo.dt_user_out_record with(nolock) where type2 = 9  and[state] = 1 and add_date >= @add_time_s and add_date <= @add_time_e group by identityid                ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOut);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select identityid, count(distinct(user_id)) as xztcnumber from dbo.dt_user_out_record with(nolock) where type2 = 10  and[state] = 1 and add_date >= @add_time_s and add_date <= @add_time_e group by identityid";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOut);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select identityid, count(distinct(user_id)) as jjnumber  from dbo.dt_user_out_record with(nolock) where type2 = 3 and state = 3 and add_date >= @add_time_s and add_date <= @add_time_e group by identityid               ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOut);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select identityid,isnull(sum(ytzmoney),0) as ytzmoney,isnull(sum(ytznumber),0) as ytznumber,isnull(sum(yzjmoney),0) as yzjmoney,isnull(sum(yzjnumber),0) as yzjnumber,isnull(sum(bysymoney),0) as bysymoney,isnull(sum(byylmoney),0) as byylmoney,isnull(sum(byylrate),0) as byylrate,isnull(sum(dlylmoney),0) as dlylmoney,isnull(sum(dlylrate),0) as dlylrate,isnull(sum(ptccmoney),0) as ptccmoney   from dbo.dt_month_record with(nolock) where add_date >=@add_time_s and add_date <=@add_time_e group by identityid";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOut);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select identityid,count(distinct(user_id)) as rewardnumber from dt_reward_userrecord with(nolock) where add_date>=@add_time_s and add_date<=@add_time_e group by identityid ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOut);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }

                querystr = "select identityid,count(distinct(user_id)) as cdnumber from dt_lottery_cancellationsflow_record with(nolock) where add_date>=@add_time_s and add_date<=@add_time_e group by identityid ";
                dt = SqlDbHelper.GetQuery(querystr, Insert_Parameter.ToArray(), sql_totalOut);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lock (StaticList.List_dt_month_all_record)
                        ModelHelper.ConvertDataRowToModelAdd(dt.Rows[i], StaticList.List_dt_month_all_record);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != "未将对象引用设置到对象的实例。")
                {
                    FillErrorMsg("syn_dt_out_account:" + ex);
                    FillErrorMsg("sql语句:" + querystr);
                    WriteErrorLog("syn_dt_out_account:" + DateTime.Now.ToString(), ex.ToString() + "\r\n" + "sql语句:" + querystr);
                }
            }
        }


        private bool CheckData(string identityid, string date_s, string date_e)
        {

            bool signal = false;
            List<SqlParameter> ListParameter = new List<SqlParameter>();
            SqlParameter datepar_s = new SqlParameter("@add_time_s", SqlDbType.Date);
            SqlParameter datepar_e = new SqlParameter("@add_time_e", SqlDbType.Date);
            SqlParameter identityidpar = new SqlParameter("@identityid", SqlDbType.VarChar, 50);
            identityidpar.Value = identityid;
            datepar_s.Value = date_s;
            datepar_e.Value = date_e;
            ListParameter.Add(datepar_s);
            ListParameter.Add(datepar_e);
            ListParameter.Add(identityidpar);
            string check_sql = "select id from dt_month_all_record where add_date>=@add_time_s and add_date<=@add_time_e and identityid=@identityid";
            DataTable dt = new DataTable();
            dt = SqlDbHelper.GetQuery(check_sql, ListParameter.ToArray(), sql_totalOrder);
            if (dt.Rows.Count == 0)
                signal = true;

            return signal;
        }

        private void RunSqlBulkCopy(DataRow dr, string tablename, string connectString)
        {
            int col_count = dr.Table.Columns.Count;
            string identityid = dr["identityid"].ToString();
            string date = dr["add_date"].ToString();
            string row_str = "insert into " + tablename;
            string col_str = "";
            string col_val = "";
            for (int i = 0; i < col_count; i++)
            {
                col_str += dr.Table.Columns[i].ColumnName;
                col_val += "'" + dr[i].ToString().TrimEnd() + "'";
                if (i != col_count - 1)
                {
                    col_val += ",";
                    col_str += ",";
                }
            }

            row_str += "(" + col_str + ")VALUES(" + col_val + ")";
            SqlDbHelper.ExecuteNonQuery(row_str, connectString);
            FillMsg("插入" + date + " -- " + identityid + " -- " + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            WriteSuccessLog("插入" + date + " -- " + identityid + " -- " + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        }

        private void RunSqlBulkUpdate(DataRow dr, string tablename, string connectString)
        {
            int col_count = dr.Table.Columns.Count;
            string row_str = "UPDATE " + tablename + " SET ";
            string identityid = dr["identityid"].ToString().TrimEnd();
            string add_date = dr["add_date"].ToString().TrimEnd();
            string col_str = "";
            string col_val = "";
            int result = 0;
            for (int i = 0; i < col_count; i++)
            {
                col_str = dr.Table.Columns[i].ColumnName;
                col_val = "'" + dr[i].ToString().TrimEnd() + "'";
                if (col_str != "sysymoney" && col_str != "syylmoney" && col_str != "syylrate")
                {
                    row_str += col_str + " = " + col_val;
                    if (i != col_count - 1)
                    {
                        row_str += ",";
                    }
                }
            }

            row_str += " WHERE identityid = '" + identityid + "' and add_date = '" + add_date + "'";
            result=SqlDbHelper.ExecuteNonQuery(row_str, connectString);
            FillMsg("修改" + add_date + " -- " + identityid + " -- " + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "影響" + result + "筆");
            WriteSuccessLog("修改" + add_date + " -- " + identityid + " -- " + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "影響" + result + "筆");
        }

        #endregion

        #region  验证各列值类型看是否加单引号
        private string ValueReset(Type type, string value)
        {
            string result = "";
            if (type.Name.Contains("Int") || type.Name == "Byte" || type.Name == "Decimal")
            {
                if (string.IsNullOrEmpty(value))
                    result = "null";
                else
                    result = value;
            }
            else
            {
                result = "'" + value + "'";
            }
            return result;
        }
        #endregion

        #region richTextBox记录
        private delegate void RichBox(string msg);
        private void FillMsg(string msg)
        {
            if (richTextBox1.InvokeRequired)
            {
                RichBox rb = new RichBox(FillMsg);
                richTextBox1.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.AppendText(msg);
                    richTextBox1.AppendText("\t\n");
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.Focus();
                }
            }
        }

        private delegate void RichBoxErr(string msg);
        private void FillErrorMsg(string msg)
        {
            if (richTextBox4.InvokeRequired)
            {
                RichBoxErr rb = new RichBoxErr(FillErrorMsg);
                richTextBox4.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox4.IsHandleCreated)
                {
                    richTextBox4.AppendText(msg);
                    richTextBox4.AppendText("\t\n");
                    richTextBox4.SelectionStart = richTextBox4.Text.Length;
                    richTextBox4.SelectionLength = 0;
                    richTextBox4.Focus();
                }
            }
        }
        #endregion

        #region 打印成功日志记录
        private object obj1 = new object();
        public void WriteSuccessLog(string msgex)
        {
            lock (obj1)
            {
                if (!File.Exists(successLogPath))
                {
                    FileStream fs1 = new FileStream(successLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.Write(msgex);
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(successLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.Write(msgex);
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 错误日志记录
        private object obj = new object();
        public void WriteErrorLog(string msgex, string msgsql)
        {
            lock (obj)
            {
                if (!File.Exists(errorLogPath))
                {
                    FileStream fs1 = new FileStream(errorLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine(msgsql);
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine(msgsql);
                    sr.WriteLine();
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 清理textbox
        private void clear_Elapse(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (System.DateTime.Now.ToString("mm") == "10")
                    ClearMsg();
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                timer_clear.Start();
            }
        }
        private delegate void RichBoxClear();
        private void ClearMsg()
        {
            if (richTextBox1.InvokeRequired )
            {
                RichBoxClear rb = new RichBoxClear(ClearMsg);
                richTextBox1.Invoke(rb);
                
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.Clear();
                   
                }
            }
        }
        #endregion

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
            Application.Exit();
            System.Environment.Exit(0);
        }

    }
}
