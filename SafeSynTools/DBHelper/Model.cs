﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
using System.Collections;

namespace SafeSynTools.DBHelper
{
    class Model
    {
        public object ConvertToMode(DataTable table, dt_month_all_record Model,ref List<dt_month_all_record> ListModel)
        {
          
            try
            {
                int row_count = table.Rows.Count;
                Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
                PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
                    {
                        dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
                    }
                }
                if (row_count > 0)
                {
                    for(int i=0;i< row_count;i++)
                    {
                        //if(ListModel.Where(m => m.identityid == table.Rows[i]["identityid"].ToString()).Count()>0)
                        //{

                        //}
                        //else
                        //{
                        //    ListModel.Add(Model);
                        //}
                        int count = table.Columns.Count;
                        for (int j = 0; j < table.Columns.Count; j++)
                        {

                            string strColumnName = table.Columns[j].ColumnName;
                            if (!dicPropertyInfo.ContainsKey(strColumnName))
                                continue;

                            object bjValue = table.Rows[i][strColumnName];
                            if (bjValue == null || bjValue is DBNull || table.Rows[0][strColumnName] == null)
                                continue;
                            
                            if (ListModel.Where(m => m.identityid == table.Rows[i]["identityid"].ToString()).Count() > 0)
                            {

                                foreach (dt_month_all_record dd in ListModel.Where(m => m.identityid == table.Rows[i]["identityid"].ToString()).ToList())
                                {
                                    dicPropertyInfo[strColumnName].SetValue(dd, bjValue, null);

                                }

                                //dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                            }
                            dicPropertyInfo[strColumnName].SetValue(Model, bjValue, null);
                            //if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "decimal")
                            //{
                            //    object bjValue2 = Convert.ToDecimal(table.Rows[0][strColumnName]);
                            //    dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                            //}
                            //else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "datetime")
                            //{

                            //    DateTime bjValue2 = Convert.ToDateTime(table.Rows[0][strColumnName]);
                            //    dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                            //}
                            //else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "double")
                            //{
                            //    double bjValue2 = Convert.ToDouble(table.Rows[0][strColumnName]);
                            //    dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                            //}
                            //else if (dicPropertyInfo[strColumnName].PropertyType.Name.ToLower() == "int32")
                            //{
                            //    int bjValue2 = Convert.ToInt32(table.Rows[0][strColumnName]);
                            //    dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                            //}
                            //else
                            //{
                            //object bjValue2 = table.Rows[0][strColumnName];
                            //dicPropertyInfo[strColumnName].SetValue(Model, bjValue2, null);
                            // }


                        }
                        if (ListModel.Where(m => m.identityid == table.Rows[i]["identityid"].ToString()).Count() == 0)
                        {
                            ListModel.Add(Model);
                        }
                    }
                    
                }
              
                    return ListModel;
            }
            catch (Exception e)
            {
                return Model;
            }


        }

        public void ConvertDataRowToModel(DataRow dataRow, object Model)
        {
            Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
            PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
                {
                    dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
                }
            }
            for (int j = 0; j < dataRow.Table.Columns.Count; j++)
            {
                string strColumnName = dataRow.Table.Columns[j].ColumnName;
                if (!dicPropertyInfo.ContainsKey(strColumnName))
                    continue;

                object bjValue = dataRow[strColumnName];
                if (bjValue == null || bjValue is DBNull)
                    continue;

                dicPropertyInfo[strColumnName].SetValue(Model, bjValue, null);
            }
        }
        public void ConvertDataRowToModelAdd(DataRow dataRow,List<dt_month_all_record> Model)
        {
            string identityid = "";
            Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
            PropertyInfo[] propertyInfos = Model[0].GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
                {
                    dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
                }
            }
            for (int j = 0; j < dataRow.Table.Columns.Count; j++)
            {
                string strColumnName = dataRow.Table.Columns[j].ColumnName;
                if (!dicPropertyInfo.ContainsKey(strColumnName))
                    continue;

                object bjValue = dataRow[strColumnName];
                if (bjValue == null || bjValue is DBNull)
                    continue;
                identityid = dataRow["identityid"].ToString();
                //dt_month_all_record model_month =  Model.Where(m => m.identityid == identityid);
                if(strColumnName != "identityid")
                foreach(dt_month_all_record month in Model.Where(m => m.identityid == identityid))
                {
                        dicPropertyInfo[strColumnName].SetValue(month, bjValue, null);
                }
               
            }
        }
        public DataTable ConvertListToDataTable<T>(IList<T> list, params string[] propertyName)
        {
            List<string> propertyNameList = new List<string>();
            if (propertyName != null)
                propertyNameList.AddRange(propertyName);

            DataTable result = new DataTable();

            PropertyInfo[] propertys = list.GetType().GetGenericArguments()[0].GetProperties();
            foreach (PropertyInfo pi in propertys)
            {
                Type type = pi.PropertyType;

                if (propertyNameList.Count == 0)
                {
                    result.Columns.Add(pi.Name, type);
                }
                else
                {
                    if (propertyNameList.Contains(pi.Name))
                        result.Columns.Add(pi.Name, type);
                }
            }
            for (int i = 0; i < list.Count; i++)
            {
                ArrayList tempList = new ArrayList();
                foreach (PropertyInfo pi in propertys)
                {
                    if (pi.PropertyType.Name.ToLower() == "object")
                        continue;

                    if (propertyNameList.Count == 0)
                    {
                        object obj = pi.GetValue(list[i], null);
                        tempList.Add(obj);
                    }
                    else
                    {
                        if (propertyNameList.Contains(pi.Name))
                        {
                            object obj = pi.GetValue(list[i], null);
                            tempList.Add(obj);
                        }
                    }
                }
                object[] array = tempList.ToArray();
                result.LoadDataRow(array, true);
            }
            return result;
        }


    }
}
