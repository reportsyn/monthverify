﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSynTools
{
    public class dt_month_all_record
    {
        public string add_date { get; set; }//站別
        public string identityid { get; set; }//站別
        public decimal czmony { get; set; }//充值金额
        public int cznumber { get; set; }//充值人数
        public decimal fastpaymoney { get; set; }//快捷支付
        public int fastnumber { get; set; } //快捷支付人数
        public decimal bankmony { get; set; }//银行转账金额
        public int banknumber { get; set; } //银行转账人数
        public decimal aliypaymony { get; set; }//支付宝金额
        public int aliypaynumber { get; set; } //支付宝人数
        public decimal wechatmony { get; set; }//微信支付
        public int wechatnumber { get; set; } //微信支付人数
        public decimal qqmoney { get; set; }//QQ支付
        public int qqnumber { get; set; } //QQ支付人数
        public decimal fourthmoney { get; set; }//第四方支付
        public int fourthnumber { get; set; } //第四方支付人数
        public decimal unionpaymoney { get; set; }//銀聯支付
        public int unionpaynumber { get; set; } //銀聯支付人数
        public decimal rgckmony { get; set; }//人工存款
        public int rgcknumber { get; set; } //人工存款人数
        public decimal rebatemoney { get; set; }//返点金额
        public int rebatenumber { get; set; } //返点人数
        public decimal hdljmony { get; set; }//活动礼金
        public int hdljnumber { get; set; } //活动礼金人数
        public decimal xthdmony { get; set; }//系统活动
        public int xthdnumber { get; set; } //系统活动人数
        public decimal qthdmony { get; set; }//其他活动
        public int qthdnumber { get; set; } //其他活动人数
        public decimal zjmoney { get; set; }//中奖金额
        public int zjnumber { get; set; } //中奖人数
        public decimal cdmony { get; set; }//撤单金额
        public int cdnumber { get; set; } //撤单人数
        public decimal dlgzmony { get; set; }//代理工资
        public int dlgznumber { get; set; } //代理工资人数
        public decimal dlfhmony { get; set; }//代理分红
        public int dlfhnumber { get; set; } //代理分红人数
        public decimal operattime { get; set; }//充值操作
        public decimal czcount { get; set; }//充值笔数
        public decimal txmony { get; set; }//提现金额
        public int txnumber { get; set; } //提现人数
        public decimal rgtcmoney { get; set; }//人工提出
        public int rgtcnumber { get; set; } //人工提出人数
        public decimal wctcmony { get; set; }//误存提出
        public int wctcnumber { get; set; } //误存提出人数
        public decimal xztcmony { get; set; }//行政提出
        public int xztcnumber { get; set; } //行政提出人数
        public decimal jjmony { get; set; }//拒绝金额
        public int jjnumber { get; set; } //拒绝金额人数
        public decimal opearttime { get; set; } //提现操作
        public decimal txcount { get; set; } //提现笔数
        public decimal tzmony { get; set; }//投注金额
        public int tznumber { get; set; } //投注人数
        public decimal tzcount { get; set; } //投注笔数
        public decimal ytzmoney { get; set; }//月投注额
        public decimal ytznumber { get; set; }//月投人數
        public decimal yzjmoney { get; set; }//月中奖额
        public decimal yzjnumber { get; set; }//月中人數
        public decimal bysymoney { get; set; }//本月损益
        public decimal sysymoney { get; set; }//上月损益
        public decimal byylmoney { get; set; }//本月盈利
        public decimal byylrate { get; set; }//本月赢率
        public decimal syylmoney { get; set; }//上月盈利
        public decimal syylrate { get; set; }//上月赢率
        public decimal dlylmoney { get; set; }//当日盈利
        public decimal dlylrate { get; set; }//当日盈率
        public decimal ptccmoney { get; set; }//平台抽成
        public decimal rewardmoney { get; set; }//打賞金额
        public int rewardcount { get; set; }//打賞次數
        public int rewardnumber { get; set; } //打賞人數

    }
}
